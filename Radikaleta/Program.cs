﻿using System;
using System.Collections.Generic;
using Nimbus;
using Nimbus.Assets;
using Nimbus.Backend;

namespace Radikaleta {
	[Requires(typeof(Camera))]
	class CameraManager : Nimbus.System {
		Transform player;
		Vector2 mapSize;

		public override void Init() {
			player = FindEntityByTag("player").GetComponent<Transform>();
			mapSize = GetEntitiesWithComponent<TiledMap>()[0].GetComponent<TiledMap>().size;
		}

		public override void Update(Entity entity) {
			if(player.position.x - entity.transform.position.x < CameraLimits.LEFT)
				entity.transform.position.x = player.position.x - CameraLimits.LEFT;
			if(player.position.x - entity.transform.position.x > CameraLimits.RIGHT)
				entity.transform.position.x = player.position.x - CameraLimits.RIGHT;
			if(player.position.y - entity.transform.position.y < CameraLimits.UP)
				entity.transform.position.y = player.position.y - CameraLimits.UP;
			if(player.position.y - entity.transform.position.y > CameraLimits.DOWN)
				entity.transform.position.y = player.position.y - CameraLimits.DOWN;

			entity.transform.position.x = Math.Max(entity.transform.position.x, 0);
			entity.transform.position.x = Math.Min(entity.transform.position.x,
			                                       (mapSize.x - MainWindow.Viewport.w) / MainWindow.PixelsPerUnit);
			entity.transform.position.y = Math.Max(entity.transform.position.y, 0);
			entity.transform.position.y = Math.Min(entity.transform.position.y,
			                                       (mapSize.y - MainWindow.Viewport.h) / MainWindow.PixelsPerUnit);
		}
	}

	class PlayerController : NimbusScript {
		public int stars;
		public int convinced;
		public int people;
		List<AudioSource> sounds;
		SpriteSheet sprite;
		bool jumping;
		bool falling;
		bool hitting;
		float jumpTime;
		bool dead;
		Vector2 forward;
		float hitTime;

		public void Hit(Vector2 force) {
			if(hitTime == 0) {
				sounds[Sounds.HIT].Play();
				rigidbody.velocity = force;
				hitTime = Time.time;
				if(stars > 0) {
					sounds[Sounds.DROP_STAR].Play();
					int numDrops = Math.Min(30, stars);
					for(int i = 0; i < numDrops; i++, stars--) {
						Entity drop = CreateEntityFromTemplate("drop-star");
						drop.transform.position = transform.position + Vector2.Up * 20;
						(drop.GetComponent<Script>().script as DropStar).velocity = new Vector2(200, 0) * Rotation.Degrees(-180 + (180 / (numDrops + 1) * (i + 1)));
					}
				}
			}
		}

		public override void Init() {
			sounds = GetComponents<AudioSource>();
			sprite = GetComponent<SpriteSheet>();
			dead = false;
		}

		void Collisions() {
			RayHit ray1 = Raycast(Vector2.Up * 1f, Vector2.Down * 6f);
			RayHit ray2 = Raycast(Vector2.Up * 1f + Vector2.Right * 5 * transform.scale.x, Vector2.Down * 6f);
			if((ray1 != null && ray1.entity.tag != "star") || (ray2 != null && ray2.entity.tag != "star")) {
				falling = false;
				forward = (ray1 != null) ? -ray1.normal.Normal : -ray2.normal.Normal;
			} else {
				rigidbody.gravity = 1;
				falling = true;
				forward = Vector2.Right;
			}

			if(CollidingWith.Find(c => c.tag == "hazard") != null) {
				sounds[Sounds.DEATH].Play();
				sprite.Disabled = false;
				rigidbody.gravity = 0.8f;
				rigidbody.velocity.y = Speeds.DEAD_JUMP;
				transform.zorder = -1000;
				entity.layerMask = LayerMask.None;
				dead = true;
			}

			if(hitting) {
				Collider hit;
				if((hit = colliders[1].CollidingWith.Find(c => c.tag == "crate")) != null) {
					sounds[Sounds.BREAK_CRATE].Play();
					DestroyEntity(hit.entity);
					Entity explosion = CreateEntityFromTemplate("explosion");
					explosion.transform.position = hit.entity.transform.position;
					explosion.animation.SetState("explosion", () => DestroyEntity(explosion));
				}
				if((hit = colliders[1].CollidingWith.Find(c => c.tag == "person")) != null) {
					sounds[Sounds.BREAK_CRATE].Play();
					people++;
					DestroyEntity(hit.entity);
					Entity explosion = CreateEntityFromTemplate("explosion");
					explosion.transform.position = hit.entity.transform.position + Vector2.Up * 8;
					explosion.animation.SetState("explosion", () => DestroyEntity(explosion));
				}
			}

			foreach(Collider star in CollidingWith.FindAll(c => c.tag == "star")) {
				sounds[Sounds.PICK_STAR].Play();
				stars++;
				DestroyEntity(star.entity);
			}
		}

		void Inputs() {
			if(!falling && !jumping && Input.Pressed("jump")) {
				jumping = true;
				jumpTime = Time.time;
				transform.position.y -= 1f;
				sounds[Sounds.JUMP].Play();
			}

			if(jumping && Input.Get("jump") && Time.time - jumpTime < 0.150f) {
				rigidbody.velocity.y = Speeds.JUMP;
			}

			if(jumping && Input.Released("jump")) {
				jumping = false;
			}

			if(!hitting) {
				if(Input.Pressed("hit")) {
					if(!jumping)
						rigidbody.velocity.x = 0;
					hitting = true;
					sounds[Sounds.HIT].Play();
				} else {
					if(Input.Get("left") && rigidbody.velocity.x > -Speeds.RUN) {
						if(transform.scale.x > 0 && rigidbody.velocity.x > Speeds.RUN - 2f && !falling)
							sounds[Sounds.DRIFT].Play();
						rigidbody.ApplyForce(-forward * 480);
						transform.scale.x = -Math.Abs(transform.scale.x);
					} 

					if(Input.Get("right") && rigidbody.velocity.x < Speeds.RUN) {
						if(transform.scale.x < 0 && rigidbody.velocity.x < -Speeds.RUN + 2f && !falling)
							sounds[Sounds.DRIFT].Play();
						rigidbody.ApplyForce(forward * 480);
						transform.scale.x = Math.Abs(transform.scale.x);
					} 
					if(!Input.Get("left") && !Input.Get("right") && Math.Abs(rigidbody.velocity.x) < 12)
						rigidbody.velocity.x = 0;
				}
			}

			if(rigidbody.velocity.y > 320)
				rigidbody.velocity.y = 320;
		}

		void Animation() {
			if(dead)
				animation.SetState("dead");
			else if(hitting)
				animation.SetState("hit", () => {
					hitting = false;
					animation.SetState(animation.GetPreviousState());
				});
			else if(falling && rigidbody.velocity.y < 0)
				animation.SetState("jump");
			else if(rigidbody.velocity.y > 0 || falling)
				animation.SetState("fall");
			else if(rigidbody.velocity.x != 0 && !falling)
				animation.SetState("walk");
			else
				animation.SetState("idle");
		}

		public override void Update() {
			if(!dead) {
				if(hitTime > 0) {
					if(Time.time - hitTime < 3)
						sprite.Disabled = !sprite.Disabled;
					else {
						hitTime = 0;
						sprite.Disabled = false;
					}
				}

				Collisions();
				Inputs();
				Animation();
			} else {
				if(transform.scale.y < 3)
					transform.scale *= new Vector2(1.05f, 1.05f);
				transform.rotation += Rotation.Degrees(12);
				if(!sounds[Sounds.DEATH].IsPlaying)
					ReplayLevel();
			}
		}
	}

	class OneWayPlatform : NimbusScript {
		Transform player;
		LayerMask block, pass;

		public override void Init() {
			player = FindEntityByTag(Tags.PLAYER).transform;
			block = LayerMask.Create(0);
			pass = LayerMask.Create(1);
		}

		public override void Update() {
			if(player.position.y <= transform.position.y)
				entity.layerMask = block;
			else
				entity.layerMask = pass;
		}
	}

	class OneWayLeftRight : NimbusScript {
		Transform player;
		LayerMask block, pass;

		public override void Init() {
			player = FindEntityByTag(Tags.PLAYER).transform;
			block = LayerMask.Create(0);
			pass = LayerMask.Create(1);
		}

		public override void Update() {
			if(player.position.x < collider.boundingBox.x2 && 
				player.position.y > transform.position.y)
				entity.layerMask = pass;
			else
				entity.layerMask = block;
		}
	}

	class Person : NimbusScript {
		int conviction;
		Collider player;
		PlayerController playerScript;
		Transform camera;
		AudioSource convinced;

		public override void Init() {
			player = FindEntityByTag(Tags.PLAYER).collider;
			playerScript = player.entity.GetComponent<Script>().script as PlayerController;
			camera = FindEntityByTag(Tags.CAMERA).transform;
			convinced = GetComponent<AudioSource>();
		}

		public override void Update() {
			if(conviction < Constants.CONVICTION) {
				RayHit rayDown = Raycast(Vector2.Up * 2 + Vector2.Right * transform.scale.x * 6, Vector2.Down * 5);
				RayHit rayRight = Raycast(Vector2.Up * 2, Vector2.Right * transform.scale.x * 10);
				if(rayDown == null || rayRight != null && (rayRight.collider.tag == "platform" || rayRight.collider.tag == "crate"))
					transform.scale.x = -transform.scale.x;
				transform.position += Vector2.Right * transform.scale.x * Speeds.WALK * Time.deltaT;

				if(colliders[1].CollidingWith.Contains(player))
					conviction++;
				if(colliders[0].CollidingWith.Contains(player))
					playerScript.Hit(new Vector2(150 * transform.scale.x, -150));
				if(conviction == Constants.CONVICTION) {
					convinced.Play();
					playerScript.convinced++;
				}
			} else {
				transform.scale.x = 1;
				transform.position.x += Speeds.RUN * 2 * Time.deltaT;
				transform.zorder = -1000;
				if(transform.position.x > camera.position.x + MainWindow.Viewport.Size.x + 20 && !convinced.IsPlaying) {
					Console.WriteLine("vou lá!");
					DestroyEntity(entity);
				}
			}
		}
	}

	class DropStar : NimbusScript {
		public Vector2 velocity;
		float creation;

		public DropStar() {
			creation = Time.time;
		}

		public override void Update() {
			if(Time.time - creation > 7) {
				DestroyEntity(entity);
				return;
			}
			if(Time.time - creation > 4)
				GetComponent<SpriteSheet>().Disabled = !GetComponent<SpriteSheet>().Disabled;
			if(Time.time - creation > 0.2f)
				entity.layerMask = LayerMask.All;
			transform.position += velocity * Time.deltaT;
			velocity += new Vector2(-velocity.x, 800) * Time.deltaT;
			RayHit ray = Raycast(Vector2.Zero, Vector2.Down * 12, LayerMask.All);
			if(ray != null && ray.collider.tag == "platform") {
				transform.position = ray.point + Vector2.Up * 5;
				velocity.y = 0;
			}
			ray = Raycast(Vector2.Zero, Vector2.Right * 12 * Math.Sign(velocity.x), LayerMask.All);
			if(ray != null && ray.collider.tag == "platform") {
				transform.position = ray.point + Vector2.Right * 5 * Math.Sign(-velocity.x);
				velocity.x = 0;
			}
		}
	}

	class GameController : NimbusScript {
		Transform camera;
		PlayerController player;
		TrueTypeFont font;
		Texture star;
		int levelStars;
		int levelPeople;
		List<AudioSource> sounds;
		bool levelEnded;
		float levelEndTime;

		public override void Init() {
			font = AssetManager.LoadTrueTypeFont("Fonts/04B_30__.TTF");
			star = AssetManager.LoadTexture("Sprites/star.png");
			camera = FindEntityByTag("camera").transform;
			player = FindEntityByTag("player").GetComponent<Script>().script as PlayerController;
			levelStars = FindEntitiesByTag("star").Count;
			levelPeople = FindEntitiesByTag("person").Count;
			sounds = GetComponents<AudioSource>();
		}

		public override void Update() {
			if(!levelEnded) {
				transform.position = player.entity.transform.position;

				if(player.entity.collider.CollidingWith.Find(c => c.tag == "hazard") != null)
					sounds[Sounds.MUSIC].Stop();

				if(player.entity.collider.CollidingWith.Find(c => c.tag == "end-level") != null) {
					sounds[Sounds.MUSIC].Stop();
					sounds[Sounds.END_LEVEL].Play();
					PersistentData.score += player.stars * 100 + player.people * 5000;
					PersistentData.convinced = player.convinced;
					player.entity.GetComponent<Script>().Disabled = true;
					player.entity.animation.Disabled = true;
					player.entity.rigidbody.Disabled = true;
					player.entity.collider.Disabled = true;
					levelEnded = true;
					levelEndTime = Time.time;
				}
			}
		}
		
		public override void LateRender() {
			string currentStars = String.Format("{0}", player.stars);
			MainWindow.Draw(star, new Rectangle(4, 3, star.rect.w, star.rect.h) + camera.position, 0, Vector2.Zero, SDL2.SDL.SDL_RendererFlip.SDL_FLIP_NONE);
			MainWindow.WriteText(font, currentStars, camera.position + new Vector2(16, 2), new Color(24, 48, 48, 255));
			if(levelEnded) {
				if(Time.time - levelEndTime > 3.5f)
					MainWindow.Fill(new Rectangle(10, 30, 180, 100) + camera.position);
				if(Time.time - levelEndTime > 4.5f) {
					string finalStars = String.Format("estrelas: {0}/{1}", player.stars, levelStars);
					MainWindow.WriteText(font, finalStars, camera.position + new Vector2(20, 50), new Color(24, 48, 48, 255));
				}
				if(Time.time - levelEndTime > 5.5f) {
					string finalPeople = String.Format("persoas: {0}/{1}", player.people, levelPeople);
					MainWindow.WriteText(font, finalPeople, camera.position + new Vector2(20, 70), new Color(24, 48, 48, 255));
				}
				if(Time.time - levelEndTime > 6.5f) {
					string score = String.Format("{0} puntos", PersistentData.score);
					MainWindow.WriteText(font, score, camera.position + new Vector2(20, 90), new Color(24, 48, 48, 255));
					if(Input.Get("hit"))
						ReplayLevel();
				}
			}
		}
	}

	class PersistentData {
		public static int score;
		public static int convinced;
	}

	class MainClass {
		public static void Main(string[] args) {
			Engine engine = new Engine("radikaleta.xml");
			engine.MainWindow.CloseEvent += (s, e) => {
				Console.WriteLine("ciao!");
				Environment.Exit(0);
			};
			engine.LoadLevel("level1");
			engine.MainLoop();
		}
	}
}
	