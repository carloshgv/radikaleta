﻿using System;

namespace Radikaleta {
	public static class Speeds {
		public static readonly float JUMP = -250f;
		public static readonly float DEAD_JUMP = -250f;
		public static readonly float RUN = 130f;
		public static readonly float WALK = 30f;
	}

	public static class Constants {
		public static readonly int CONVICTION = 80;
	}

	public static class CameraLimits {
		public static readonly float LEFT = 80f;
		public static readonly float RIGHT = 100f;
		public static readonly float UP = 40f;
		public static readonly float DOWN = 120f;
	}

	public static class Sounds {
		public static readonly int JUMP = 0;
		public static readonly int HIT = 1;
		public static readonly int DEATH = 2;
		public static readonly int PICK_STAR = 3;
		public static readonly int DROP_STAR = 4;
		public static readonly int DRIFT = 5;
		public static readonly int BREAK_CRATE = 6;
		public static readonly int MUSIC = 0;
		public static readonly int END_LEVEL = 1;
	}

	public static class Tags {
		public static readonly string PLAYER = "player";
		public static readonly string CAMERA = "camera";
	}
}

